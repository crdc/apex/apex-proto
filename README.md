# Apex Protobuf

## Develop

Setup protobuf compilers and tools

```sh
pacman -S protobuf
go get -u github.com/ckaznocha/protoc-gen-lint
go get -u github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc
```

Lint with

```sh
utils/lint
```

If there is an error during this the files will be left in a `lint`
directory, otherwise they are cleaned up.

Generate documentation with

```sh
utils/doc
```

## Rust

To perform serialization and deserialization of a generated struct

```sh
impl Plugin {
    fn serialize(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.reserve(self.encoded_len());
        self.encode(&mut buf).unwrap();
        buf
    }

    fn deserialize(&self, buf: &[u8]) -> Result<Plugin, prost::DecodeError> {
        Plugin::decode(&mut Cursor::new(buf))
    }
}
```

## Go

## Python

## C

## Vala (?)
